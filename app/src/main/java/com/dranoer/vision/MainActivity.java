package com.dranoer.vision;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.bumptech.glide.Glide;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.dranoer.vision.ui.camera.CameraSourcePreview;

import java.lang.ref.WeakReference;
import java.util.Set;

public class MainActivity extends AppCompatActivity
{
	public static final String TAG = "SmileD";
	public HandlerThread ScreenBrightnessHT = null;
	public Handler ScreenBrightnessH;
	public int ScreenThreadTime = 30000;

	public UsbService usbService;

	public CameraSource cameraSource = null;
	public CameraSourcePreview cameraSourcePreview;

	public WindowManager.LayoutParams params;

	public TextView SmilePercText;
	public Button Sending1ToEspButton; //DEVELOP ONLY
	RoundCornerProgressBar smileProgressBar;

	public double smileThreshold = 0.5;
	public float [] smileSequence = {0,0,0,0,0,0,0,0,0};
	public int smileSequenceIndex = 0;

	public int faceHeightThreshold = 200;
	boolean onFaceProcess = false;

	public final Animation animation = new AlphaAnimation(1,0);
	public USBHandler usbHandler;

	static Camera camera = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

		usbHandler = new USBHandler(this);

		//DEVELOP ONLY START
		animation.setDuration(50);
		animation.setInterpolator(new LinearInterpolator());
		animation.setRepeatCount(Animation.INFINITE);
		animation.setRepeatMode(Animation.REVERSE);
		//DEVELOP ONLY END

		View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);

		faceHeightThreshold = (int)(getImageHeight()/5.6);

	    cameraSourcePreview = findViewById(R.id.preview);
	    ImageView smileGif = findViewById(R.id.SmileGif);
	    SmilePercText = findViewById(R.id.SmilePercText);
	    smileProgressBar = findViewById(R.id.smileProgress);
		Sending1ToEspButton = findViewById(R.id.Sending1ToEsp);

	    smileProgressBar.setProgress(0);
	    smileProgressBar.setSecondaryProgress(0);

	    createCameraSource();
	    Glide.with(getApplicationContext())
			    .load(R.drawable.smile)
			    .into(smileGif);

	    if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
	    {
		    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
		    {
			    requestPermissions(new String[]{Manifest.permission.CAMERA} , 5);
		    }
	    }
	    if(ScreenBrightnessHT == null)
	    {
		    ScreenBrightnessHT = new HandlerThread("screen Brightness thread");
		    ScreenBrightnessHT.start();
		    ScreenBrightnessH = new Handler(ScreenBrightnessHT.getLooper());
		    ScreenBrightnessH.postDelayed(new Runnable()
		    {
			    @Override
			    public void run()
			    {
				    runOnUiThread(new Runnable()
				    {
					    @Override
					    public void run()
					    {
						    if(!onFaceProcess)
						    {
							    params = getWindow().getAttributes();
							    params.screenBrightness = 0.1f;
							    getWindow().setAttributes(params);
							    smileProgressBar.setProgress(0);
							    smileProgressBar.setSecondaryProgress(0);
						    }
						    onFaceProcess = false;
					    }
				    });
				    ScreenBrightnessH.postDelayed(this , ScreenThreadTime);
			    }
		    } , ScreenThreadTime);
	    }
    }

	@Override
	protected void onResume()
	{
		super.onResume();
		startCameraSource();
		setFilters();
		startService(UsbService.class, usbConnection , null);
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		cameraSourcePreview.stop();
		unbindService(usbConnection);
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		if(cameraSource != null)
		{
			cameraSource.release();
		}
	}

	public void startService(Class<?> service , ServiceConnection serviceConnection , Bundle extras)
	{
		if(!UsbService.SERVICE_CONNECTED)
		{
			Intent startService = new Intent(this , service);
			if(extras != null && !extras.isEmpty())
			{
				Set<String> keys = extras.keySet();
				for(String key: keys)
				{
					String extra = extras.getString(key);
					startService.putExtra(key , extra);
				}
			}
			startService(startService);
		}
		Intent bindingIntent = new Intent(this, service);
		bindService(bindingIntent , serviceConnection , Context.BIND_AUTO_CREATE);
	}
	public final BroadcastReceiver mUsbReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			switch (intent.getAction())
			{
				case UsbService.ACTION_USB_PERMISSION_GRANTED:
					Toast.makeText(context , "USB is Ready" , Toast.LENGTH_SHORT).show();
					break;
				case UsbService.ACTION_USB_PERMISSION_NOT_GRANTED:
					Toast.makeText(context , "USB Permission Not granted" , Toast.LENGTH_SHORT).show();
					break;
				case UsbService.ACTION_NO_USB:
					Toast.makeText(context , "No USB Connected" , Toast.LENGTH_SHORT).show();
					break;
				case UsbService.ACTION_USB_DISCONNECTED:
					Toast.makeText(context , "USB Disconnected" , Toast.LENGTH_SHORT).show();
					break;
				case UsbService.ACTION_USB_NOT_SUPPORTED:
					Toast.makeText(context , "USB Device Not supported" , Toast.LENGTH_SHORT).show();
					break;
			}
		}
	};
    public void setFilters()
    {
	    IntentFilter filter = new IntentFilter();
	    filter.addAction(UsbService.ACTION_USB_PERMISSION_GRANTED);
	    filter.addAction(UsbService.ACTION_NO_USB);
	    filter.addAction(UsbService.ACTION_USB_DISCONNECTED);
	    filter.addAction(UsbService.ACTION_USB_NOT_SUPPORTED);
	    filter.addAction(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED);
	    registerReceiver(mUsbReceiver , filter);
    }

	public final ServiceConnection usbConnection = new ServiceConnection()
    {
	    @Override
	    public void onServiceConnected(ComponentName name, IBinder service)
	    {
			usbService = ((UsbService.UsbBinder) service).getService();
			usbService.setHandler(usbHandler);
	    }

	    @Override
	    public void onServiceDisconnected(ComponentName name)
	    {
			usbService = null;
	    }
    };

    public static class USBHandler extends Handler
    {
    	private final WeakReference<MainActivity> mainActivityWeakReference;
    	public USBHandler(MainActivity activity)
	    {
		    mainActivityWeakReference = new WeakReference<>(activity);
	    }
	    @Override
	    public void handleMessage(Message msg)
	    {

	    }
    }

    public FaceDetector createFaceDetector(Context context)
    {
    	FaceDetector detector = new FaceDetector.Builder(context)
			    .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
			    .setMode(FaceDetector.FAST_MODE)
			    .setLandmarkType(FaceDetector.NO_LANDMARKS)
			    .setTrackingEnabled(true)
			    .setMinFaceSize(0.10f)
			    .build();
    	detector.setProcessor(
    			new MultiProcessor.Builder<>(new mlibreFTF()).build()
	    );
    	return detector;
    }
    public void createCameraSource()
    {
	    Context context = getApplicationContext();
	    FaceDetector detector = createFaceDetector(context);

		if(!detector.isOperational())
		{
			Log.d(TAG, "createCameraSource: Face Detector is not operational");
		}
	    cameraSource = new CameraSource.Builder(context , detector)
			    .setFacing(CameraSource.CAMERA_FACING_FRONT)
			    .setRequestedFps(5.0f)//set to 5. because of darkness bug in higher frame rate in some cases.
			    .build();
    }
    public void startCameraSource()
    {
    	if(cameraSource != null)
	    {
	    	try
		    {
		    	cameraSourcePreview.start(cameraSource);
		    }
		    catch (Exception e)
		    {
			    Log.d(TAG, "startCameraSource: CameraSourcr problem");
			    cameraSource.release();
			    cameraSource = null;
		    }
	    }
    }

	private class mlibreFTF implements MultiProcessor.Factory<Face>
	{
		@Override
		public Tracker<Face> create(Face face)
		{
			return new mlibreFT();
		}
	}

	private class mlibreFT extends Tracker<Face>
	{
		@Override
		public void onUpdate(FaceDetector.Detections<Face> detections, Face face)
		{
			onFaceProcess = true;
			final float tmp = face.getIsSmilingProbability();
			//DEVELOP ONLY START
			boolean YESSEND = false;
			//DEVELOP ONLY END
			if(tmp > smileThreshold && face.getHeight() > faceHeightThreshold)
			{
				if(usbService != null)
				{
					usbService.write("1".getBytes());
					//DEVELOP ONLY START
					YESSEND = true;
					//DEVELOP ONLY END

				}
			}
			//DEVELOP ONLY START
			final boolean finalYESSEND = YESSEND;
			//DEVELOP ONLY END
			runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					//DEVELOP ONLY START
					if(finalYESSEND)
					{
						Sending1ToEspButton.startAnimation(animation);
//						Sending1ToEspButton.clearAnimation();
					}
					else
					{
						Sending1ToEspButton.clearAnimation();
					}
					//DEVELOP ONLY END
					params = getWindow().getAttributes();
					params.screenBrightness = 1f;
					getWindow().setAttributes(params);

					smileSequence[(smileSequenceIndex++)%smileSequence.length] = tmp;
					float ave = 0;
					for(float d : smileSequence) ave += d;
					ave = ave/smileSequence.length;

					int smilePInt = Math.round(ave*100);
					if(smilePInt < 5)
					{
						smilePInt = 0;
					}
					SmilePercText.setText(String.format("%s%s", " %", Integer.toString(smilePInt)));
					if(ave > 0.5)
					{
						smileProgressBar.setProgress(50);
						smileProgressBar.setSecondaryProgress(ave*100);
					}
					else
					{
						smileProgressBar.setSecondaryProgress(0);
						smileProgressBar.setProgress(ave*100);
					}
				}
			});
		}
		@Override
		public void onDone()
		{

		}
	}

	public float getFrontCameraRes()
	{
		float maxRes = -1;
		long pixelCount = -1;
		Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
		Camera.getCameraInfo(1 , cameraInfo);
		Camera camera = Camera.open(1);
		Camera.Parameters cameraParams = camera.getParameters();
		for(int i = 0 ; i < cameraParams.getSupportedPictureSizes().size() ; i++)
		{
			long temp = cameraParams.getSupportedPictureSizes().get(i).width * cameraParams.getSupportedPictureSizes().get(i).height;
			if(temp > pixelCount)
			{
				maxRes = ((float)temp) / (1024000.0f);
			}
		}
		camera.release();
		return maxRes;
	}

	public float getImageHeight()
	{
		Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
		Camera.getCameraInfo(1 , cameraInfo);
		Camera camera = Camera.open(1);
		Camera.Parameters cameraParams = camera.getParameters();
		float hei = cameraParams.getSupportedPictureSizes().get(1).height;
		camera.release();
		return hei;
	}
}
