package com.dranoer.vision;

/**
 * Created by mlibre on 12/02/18.
 */

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

@GlideModule
public final class MyAppGlideModule extends AppGlideModule {}

// copied from http://bumptech.github.io/glide/doc/generatedapi.html